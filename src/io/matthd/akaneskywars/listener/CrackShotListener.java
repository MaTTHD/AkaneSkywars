package io.matthd.akaneskywars.listener;

import com.shampaggon.crackshot.events.WeaponDamageEntityEvent;
import io.matthd.akaneskywars.SkywarsPlugin;
import io.matthd.akaneskywars.game.LocalGameHandler;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/**
 * Created by Matthew on 2016-02-03.
 */
public class CrackShotListener implements Listener {

    private SkywarsPlugin plugin = SkywarsPlugin.getInstance();

    @EventHandler
    public void onDamage(WeaponDamageEntityEvent e){
        Entity damage = e.getVictim();
        Entity damager = e.getDamager();

        if(!(damage instanceof Player) && !(damager instanceof Player)){
            return;
        }

        Player attacker = (Player) e.getDamager();
        Player vitcim = (Player) e.getVictim();

        if(vitcim.getHealth() - e.getDamage() <= 0){
            //Died
            plugin.getLocalGameHandler().handleDeath(vitcim, attacker);
        }
        else {
            return;
        }
    }
}
