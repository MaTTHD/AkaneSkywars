package io.matthd.akaneskywars.kits;

import io.matthd.akaneskywars.SkywarsPlugin;
import io.matthd.akaneskywars.game.Skywars;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matthew on 2016-01-31.
 */
public class Weapon {

    List<ItemStack> guns = new ArrayList<>();

    private String name;
    private ItemStack ammo;

    public Weapon(String name){
        this.name = name;
        for(String s : SkywarsPlugin.getInstance().getWeapons().getStringList(name)){
            String[] split = s.split(",");
            ItemStack item = new ItemStack(Material.getMaterial(Integer.valueOf(split[0])));
            guns.add(item);
        }
        ammo = new ItemStack(Material.getMaterial(Integer.valueOf(SkywarsPlugin.getInstance().getWeapons().getString(name + ".ammo"))));
    }

    public List<ItemStack> getGuns() {
        return guns;
    }

    public void setGuns(List<ItemStack> guns) {
        this.guns = guns;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ItemStack getAmmo(){
        return ammo;
    }
}
