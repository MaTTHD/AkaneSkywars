package io.matthd.akaneskywars.kits;

import io.matthd.akaneskywars.SkywarsPlugin;
import io.matthd.core.gameapi.kit.Kit;
import io.matthd.core.gameapi.player.ArcadePlayer;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Matthew on 2016-01-29.
 */
public class SkywarsKit implements Kit {

    private SkywarsPlugin plugin = SkywarsPlugin.getInstance();

    private String name;
    private List<ItemStack> items;
    private List<ItemStack> armor;
    private String[] desc;
    private ItemStack displayItem;


    public SkywarsKit(String name){
        this.name = name;
        ItemStack primary = new ItemStack(Material.getMaterial(Integer.valueOf(plugin.getKits().getString(name + ".primary"))));
        ItemStack secondary = new ItemStack(Material.getMaterial(Integer.valueOf(plugin.getKits().getString(name + ".primary"))));

        this.items.add(primary);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public ItemStack[] getArmor() {
        return (ItemStack[]) armor.toArray();
    }

    @Override
    public ItemStack[] getItems() {
        return (ItemStack[]) items.toArray();
    }

    @Override
    public void giveKit(ArcadePlayer arcadePlayer) {

    }

    @Override
    public ItemStack getDisplayItem() {
        return displayItem;
    }

    @Override
    public String[] getDesc() {
        return desc;
    }

    @Override
    public void setItems(ItemStack[] itemStacks) {
        items.clear();
        items = Arrays.asList(itemStacks);
    }

    @Override
    public void setName(String s) {
        this.name = s;
    }

    @Override
    public void setDisplayItem(ItemStack itemStack) {
        this.displayItem = itemStack;
        SkywarsPlugin.getInstance().getKits().set(name + ".displayItem", itemStack.getType().getId() + "," + itemStack.getAmount());
        SkywarsPlugin.getInstance().saveKits();
    }

    @Override
    public void setDesc(String[] strings) {

    }

    @Override
    public void setArmor(ItemStack[] itemStacks) {
        armor.clear();
        armor = Arrays.asList(itemStacks);
    }
}
