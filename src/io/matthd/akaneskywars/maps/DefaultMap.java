package io.matthd.akaneskywars.maps;

import io.matthd.core.gameapi.game.GameMap;
import org.bukkit.World;

import java.io.File;

/**
 * Created by Matthew on 2016-01-16.
 */
public class DefaultMap extends GameMap {

    public DefaultMap(World world) {
        super(world);
    }
}
