package io.matthd.akaneskywars.listener;

import io.matthd.akaneskywars.SkywarsPlugin;
import io.matthd.akaneskywars.kits.Weapon;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Matthew on 2016-02-03.
 */
public class InventoryListener implements Listener {

    private SkywarsPlugin plugin = SkywarsPlugin.getInstance();

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e){
        if(e.getSlot() == 0){
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onItemThrow(PlayerDropItemEvent e){
        Player pl = e.getPlayer();

        Weapon wep = plugin.getGunManager().getGunForPlayer(pl);
        wep.getGuns().stream().filter(item -> e.getItemDrop().getItemStack().getType() == item.getType()).forEach(item -> e.setCancelled(true));
    }

    @EventHandler
    public void onItemPickup(PlayerPickupItemEvent e){
        Player pl = e.getPlayer();
        Weapon wep = plugin.getGunManager().getGunForPlayer(pl);
        ItemStack ammo = wep.getAmmo();

        if(e.getItem().getItemStack().getType() == ammo.getType()){
            e.setCancelled(true);
        }
    }
}
