package io.matthd.akaneskywars.game;

import io.matthd.akaneskywars.SkywarsPlugin;
import io.matthd.akaneskywars.kits.SkywarsKit;
import io.matthd.core.gameapi.player.ArcadePlayer;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Matthew on 2016-01-17.
 */
public class LocalKitHandler {

    private SkywarsPlugin plugin = SkywarsPlugin.getInstance();

    private List<SkywarsKit> allKits = new ArrayList<>();
    private Map<ArcadePlayer, SkywarsKit> playerKits = new HashMap<>();
    
    public void setup(){
        for(String name : plugin.getKits().getConfigurationSection("kits").getKeys(false)){
            SkywarsKit kit = new SkywarsKit(name);
            allKits.add(kit);
        }
    }

    private void giveKits(){
        for(Map.Entry<ArcadePlayer, SkywarsKit> entry : playerKits.entrySet()){
            ItemStack item = new ItemStack(Material.SKULL_ITEM, 1);
            SkullMeta meta = (SkullMeta) item.getItemMeta();

            meta.setOwner("MHF_Chest");
            item.setItemMeta(meta);
            entry.getKey().getPlayer().getInventory().setItem(0, item);
        }
    }

    public void start(){
        giveKits();
    }
}
