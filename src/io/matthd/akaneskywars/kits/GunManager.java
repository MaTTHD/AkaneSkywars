package io.matthd.akaneskywars.kits;

import com.shampaggon.crackshot.CSUtility;
import io.matthd.akaneskywars.SkywarsPlugin;
import io.matthd.akaneskywars.game.LocalGameHandler;
import io.matthd.core.Core;
import io.matthd.core.backend.player.CorePlayer;
import io.matthd.core.gameapi.player.ArcadePlayer;
import io.matthd.core.gameapi.player.PlayerManager;
import net.minecraft.server.v1_8_R3.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Matthew on 2016-01-31.
 */
public class GunManager {

    private SkywarsPlugin plugin = SkywarsPlugin.getInstance();

    private Map<ArcadePlayer, Weapon> guns = new HashMap<>();
    private List<Weapon> allGuns = new ArrayList<>();
    private Map<ArcadePlayer, Integer> tier = new HashMap<>();

    public void load() {
        allGuns.addAll(plugin.getWeapons().getKeys(false).stream().map(Weapon::new).collect(Collectors.toList()));
    }

    public void start() {
        plugin.getLocalGameHandler().getAlive().stream().filter(guns::containsKey).forEach(pl -> tier.put(pl, 0));
    }

    public Map<ArcadePlayer, Weapon> getGuns() {
        return guns;
    }

    public Weapon getGunForPlayer(Player player) {
        for (Map.Entry<ArcadePlayer, Weapon> entry : guns.entrySet()) {
            if (entry.getKey().getPlayer().getName().equalsIgnoreCase(player.getName())) {
                return entry.getValue();
            }
        }
        return null;
    }

    public ItemStack getGunItemForPlayer(Player player) {
        Weapon wep = getGunForPlayer(player);
        return wep.getGuns().get(tier.get(PlayerManager.getInstance().getPlayer(player.getName())));
    }

    public void nextGun(Player player){
        Weapon wep = getGunForPlayer(player);
        int index = currentTier(player);

        int nextGun = index + 1;
        tier.put(PlayerManager.getInstance().getPlayer(player.getName()), nextGun);
        player.getInventory().setItem(0, wep.getGuns().get(nextGun));
    }

    public int currentTier(Player player){
        for(Map.Entry<ArcadePlayer, Integer> entry : tier.entrySet()){
            if(entry.getKey().getPlayer().getName().equalsIgnoreCase(player.getName())){
                return entry.getValue();
            }
        }
        return 0;
    }
}
