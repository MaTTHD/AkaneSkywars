package io.matthd.akaneskywars.game;


import io.matthd.core.gameapi.GameCore;
import io.matthd.core.gameapi.game.ArcadeGame;
import io.matthd.core.gameapi.game.GameMap;
import io.matthd.core.gameapi.kit.Kit;
import io.matthd.core.gameapi.timer.GameTimer;

/**
 * Created by Matthew on 2016-01-15.
 */
public class Skywars extends GameCore {

    private GameTimer timer;

    public Skywars(){
        setupGame();
        timer = new GameTimer(60, 8*60);
    }

    @Override
    public ArcadeGame setupGame() {
        ArcadeGame game = new ArcadeGame("Skywars", registerMaps(), registerKits());
        return game;
    }

    @Override
    public Kit[] registerKits() {


        return new Kit[0];
    }

    @Override
    public GameMap[] registerMaps() {
        return new GameMap[0];
    }

    public GameTimer getTimer() {
        return timer;
    }
}
