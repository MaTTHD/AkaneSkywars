package io.matthd.akaneskywars.game;

import io.matthd.akaneskywars.SkywarsPlugin;
import io.matthd.core.Messages;
import io.matthd.core.gameapi.events.*;
import io.matthd.core.gameapi.game.ArcadeHandler;
import io.matthd.core.gameapi.game.GameState;
import io.matthd.core.gameapi.player.PlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/**
 * Created by Matthew on 2016-01-16.
 */
public class GameListener implements Listener {

    private SkywarsPlugin plugin = SkywarsPlugin.getInstance();

    @EventHandler
    public void onDeath(ArcadePlayerDamageByPlayerEvent e){
        Player pl = e.getPlayer();
        Player damager = e.getDamager();
        double damage = e.getDamage();
        if((pl.getHealth() - damage) <= 0){
            //DIED
            plugin.getLocalGameHandler().handleDeath(pl, damager);
        }
    }

    @EventHandler
    public void onJoin(ArcadePlayerJoinEvent e){
        Player pl = e.getPlayer();
        Bukkit.broadcastMessage(Messages.EVENT_JOIN.get());
        if((int) Properties.MIN_PLAYERS.get() > Bukkit.getOnlinePlayers().size()){

        }
        else {
            plugin.getGame().getTimer().run();
            Bukkit.broadcastMessage(Messages.EVENT_START.get().replace("%time%", Integer.toString(plugin.getGame().getTimer().getLobbySeconds())));
        }
    }

    @EventHandler
    public void onTick(ArcadeTickEvent e){
        GameState state = e.getState();
        if(state == GameState.LOBBY){
            if((int) Properties.MIN_PLAYERS.get() > Bukkit.getOnlinePlayers().size()){
                return;
            }
            Bukkit.broadcastMessage(Messages.EVENT_TICK.get().replace("%time%", Integer.toString(e.getSecond())));
        }
    }
    @EventHandler
    public void onPlayerLeave(ArcadePlayerLeaveEvent e){
        Player pl = e.getPlayer();
        ArcadeHandler.getInstance().getCurrentGame().getAllPlayers().remove(PlayerManager.getInstance().getPlayer(pl.getName()));
        int current = plugin.getLocalGameHandler().getAlive().size();
        if(current == 1){
            ArcadeHandler.getInstance().stopGame();
        }
    }
}
