package io.matthd.akaneskywars.listener;

import io.matthd.akaneskywars.SkywarsPlugin;
import io.matthd.akaneskywars.game.Properties;
import io.matthd.core.backend.listeners.PlayerJoin;
import io.matthd.core.gameapi.player.PlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Skull;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Matthew on 2016-02-04.
 */
public class PlayerListener implements Listener {

    private SkywarsPlugin plugin = SkywarsPlugin.getInstance();

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e){
        Player pl = e.getPlayer();
        plugin.getLocalGameHandler().getAlive().add(PlayerManager.getInstance().getPlayer(pl.getName()));

        if(Bukkit.getOnlinePlayers().size() > (int)Properties.MIN_PLAYERS.get()){
            plugin.getLocalGameHandler().startGame();
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e){
        Player pl = e.getPlayer();
        plugin.getLocalGameHandler().getAlive().add(PlayerManager.getInstance().getPlayer(pl.getName()));

        if(plugin.getLocalGameHandler().getAlive().size() <= 1){
            plugin.getLocalGameHandler().stopGame();
        }
    }

    @EventHandler
    public void onPlayerPlaceItem(BlockPlaceEvent e){
        Player pl = e.getPlayer();
        Block b = e.getBlockPlaced();
        Material item = b.getType();
        ItemStack itemStack = new ItemStack(item, 1);
        if(item == Material.SKULL_ITEM){
            SkullMeta skullMeta = (SkullMeta) itemStack.getItemMeta();
            String name = skullMeta.getOwner();

            if(name.equalsIgnoreCase("MHF_Chest")){
                ArmorStand stand = pl.getWorld().spawn(b.getLocation(), ArmorStand.class);
                stand.setVisible(false);

                new BukkitRunnable(){
                    int tick = 5;
                    public void run(){
                        if(tick > 0){
                            tick--;
                            stand.setCustomName(ChatColor.GREEN + "" + tick + "");
                            stand.setCustomNameVisible(true);
                        }
                        else {
                            cancel();
                            stand.remove();
                            skullMeta.setOwner("AKN_Chest");
                            itemStack.setItemMeta(skullMeta);
                            b.getState().update();
                        }
                    }
                }.runTaskTimer(SkywarsPlugin.getInstance(), 0L, 20L);
            }
        }
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e){
        Player pl = e.getPlayer();

        if(e.getAction() != Action.RIGHT_CLICK_BLOCK || e.getAction() != Action.LEFT_CLICK_BLOCK){
            return;
        }

        Block block = e.getClickedBlock();
        if(block instanceof Skull){
            Skull skull = (Skull) block.getState();
            if(plugin.getLocalGameHandler().getLoadedChests().contains(skull.getLocation())){
                //Contains

            }
        }
    }
}
