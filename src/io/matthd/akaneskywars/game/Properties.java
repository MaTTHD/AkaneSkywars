package io.matthd.akaneskywars.game;

import io.matthd.akaneskywars.SkywarsPlugin;

/**
 * Created by Matthew on 2016-01-16.
 */
public enum Properties {
    MIN_PLAYERS(SkywarsPlugin.getInstance().getGame().setupGame().getCurrentMap().getSpawns().size())
    ;

    private Object value;
    Properties(Object value){
        this.value = value;
    }

    public Object get(){
        return value;
    }
}
