package io.matthd.akaneskywars.game;

import io.matthd.akaneskywars.SkywarsPlugin;
import io.matthd.core.Core;
import io.matthd.core.Messages;
import io.matthd.core.backend.player.PlayerGeneral;
import io.matthd.core.gameapi.events.ArcadePlayerDamageByPlayerEvent;
import io.matthd.core.gameapi.game.ArcadeHandler;
import io.matthd.core.gameapi.game.GameMap;
import io.matthd.core.gameapi.player.ArcadePlayer;
import io.matthd.core.gameapi.player.PlayerManager;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Skull;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

/**
 * Created by Matthew on 2016-01-16.
 */
public class LocalGameHandler {

    private Map<String, Integer> rewards = new HashMap<>();
    private List<ArcadePlayer> alive = new ArrayList<>();
    private List<ArcadePlayer> spectating = new ArrayList<>();

    private Map<Location, Inventory> loadedChests = new HashMap<>();

    private SkywarsPlugin plugin = SkywarsPlugin.getInstance();

    private List<Location> cageLocations = new ArrayList<>();

    public List<ArcadePlayer> getAlive() {
        return alive;
    }

    public void stopGame(){
        Player winner = alive.get(0).getPlayer();
        Bukkit.broadcastMessage(Messages.EVENT_END.get().replace("%player%", winner.getName()));

        for(Map.Entry<String, Integer> entry : rewards.entrySet()){
            Player pl = Bukkit.getPlayer(entry.getKey());
            if(!pl.isOnline()){
                OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(entry.getKey());
                PlayerGeneral pg = PlayerGeneral.get(offlinePlayer.getUniqueId());
                pg.setCoins(pg.getCoins() + entry.getValue());
            }
            else {
                PlayerGeneral pg = PlayerGeneral.get(pl.getUniqueId());
                pg.setCoins(pg.getCoins() + entry.getValue());
                pl.sendMessage(Messages.PLAYER_WIN_COINS.get().replace("%coins%", String.valueOf(entry.getValue())));
            }
        }

        ArcadeHandler.getInstance().stopGame();
    }

    public void startGame(){
        for(Location loc : SkywarsPlugin.getInstance().getGame().setupGame().getCurrentMap().getSpawns()){
            for(int x = loc.getBlockX(); x < loc.getBlockX() + 5; x++){
                for(int y = loc.getBlockY(); y < loc.getBlockY() + 5; y++){
                    for(int z = loc.getBlockZ(); z < loc.getBlockZ() +5; z++){
                        if(loc.getWorld().getBlockAt(x,y,z).getType() == Material.GLASS){
                            cageLocations.add(new Location(loc.getWorld(), x,y,z));
                        }
                    }
                }
            }
        }
        Bukkit.broadcastMessage(Messages.EVENT_START.get());
        plugin.getKitHandler().start();

        new BukkitRunnable(){
            public void run(){
                loadedChests.clear();
                GameMap currentMap = plugin.getGame().setupGame().getCurrentMap();
                World gameWorld = currentMap.getWorld();

                for(Chunk c : gameWorld.getLoadedChunks()){
                    for(Entity e : c.getEntities()){
                        if(e instanceof Skull){
                            Skull skull = (Skull) e;
                            if(skull.getOwner().equalsIgnoreCase("AKN_Chest")){
                                loadedChests.put(skull.getLocation(), stockInventory());
                            }
                        }
                    }
                }
            }
        }.runTaskTimer(plugin, 0L, 3 * 20L);
    }

    public Inventory stockInventory(){
        Random ran = new Random();
        Inventory inv = Bukkit.createInventory(null, 54, "Chest");
        for(int i = 0; i < 5; i++){
            int toChoose = ran.nextInt(plugin.getConfig().getStringList("crates").size());
            ItemStack item = new ItemStack(Material.getMaterial(plugin.getConfig().getStringList("crates").get(toChoose).split(",")[0]), Integer.valueOf(plugin.getConfig().getStringList("crates").get(toChoose).split(",")[1]));
            inv.setItem(ran.nextInt(inv.getSize()), item);
        }
        return inv;
    }

    public List<ArcadePlayer> getSpectating() {
        return spectating;
    }

    public Map<String, Integer> getRewards() {
        return rewards;
    }

    public void handleDeath(Player pl, Player killer){
        if(rewards.containsKey(pl.getName())){
            rewards.put(pl.getName(), rewards.get(pl.getName()) + 10);
        }
        else {
            rewards.put(pl.getName(), 10);
        }
        alive.remove(PlayerManager.getInstance().getPlayer(pl.getName()));
        spectating.add(PlayerManager.getInstance().getPlayer(pl.getName()));
        Bukkit.broadcastMessage(Messages.PLAYER_DEATH.get().replace("%killer%", killer.getName()).replace("%dead%", pl.getName()));

        //STOP GAME IF END

        if(alive.size() > 1){
            return;
        }
        else {
            stopGame();
        }
    }

    public Map<Location, Inventory> getLoadedChests() {
        return loadedChests;
    }
}
