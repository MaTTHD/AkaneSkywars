package io.matthd.akaneskywars;

import com.shampaggon.crackshot.CSUtility;
import io.matthd.akaneskywars.game.LocalGameHandler;
import io.matthd.akaneskywars.game.LocalKitHandler;
import io.matthd.akaneskywars.game.Skywars;
import io.matthd.akaneskywars.kits.GunManager;
import io.matthd.core.modular.Module;
import io.matthd.core.modular.ModuleMeta;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

/**
 * Created by Matthew on 2016-01-15.
 */

@ModuleMeta(name = "Skywars",
description = "Skywars plugin")
public class SkywarsPlugin extends Module {

    private static SkywarsPlugin instance;
    private Skywars game;
    private LocalGameHandler lgh;
    private GunManager gunManager;
    private LocalKitHandler kitHandler;

    private File weaponsFile;
    private FileConfiguration weapons;

    private File kitsFile;
    private FileConfiguration kits;

    @Override
    protected void onModuleEnable() throws Exception {
        instance = this;
        game = new Skywars();
        lgh = new LocalGameHandler();
        kitHandler = new LocalKitHandler();

        if(!weaponsFile.exists()){
            weaponsFile.createNewFile();
        }
        if(!kitsFile.exists()){
            kitsFile.createNewFile();
        }

        weapons = YamlConfiguration.loadConfiguration(weaponsFile);
        kits = YamlConfiguration.loadConfiguration(kitsFile);
    }

    public FileConfiguration getKits() {
        return kits;
    }

    public FileConfiguration getWeapons() {
        return weapons;
    }

    @Override
    protected void onModuleDisable() throws Exception {
        instance = null;
    }

    public void saveKits(){
        try {
            kits.save(kitsFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveWeapons(){
        try {
            weapons.save(weaponsFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static SkywarsPlugin getInstance() {
        return instance;
    }

    public Skywars getGame(){
        return game;
    }

    public LocalGameHandler getLocalGameHandler() {
        return lgh;
    }

    public GunManager getGunManager(){
        return gunManager;
    }

    public LocalKitHandler getKitHandler() {
        return kitHandler;
    }
}
